import request from '@/utils/request'


export default{
  // 列表查询分页
  // current当前页
  // size每页记录数
  getLinkmanList(current,size){
    return request({
      url: `/tbl-linkman/pageLinkman/${current}/${size}`,
      method: 'get'
    })
  },
  // 删除供应商联系人
  deleteLinkmanById(id){
    return request({
      url: `/tbl-linkman/${id}`,
      method: 'delete'
    })
  },
  // 修改供应商联系人信息
  updateLinkman(linkman){
    return request({
      url: `/tbl-linkman/updateLinkman`,
      method: 'post',
      data: linkman
    })
  },
  // 添加联系人
  addLinkman(linkman){
    return request({
      url: `/tbl-linkman/addLinkman`,
      method: 'post',
      data: linkman
    })
  },
  // 修改供应商零件信息
  getLinkmanById(id){
    return request({
      url: `/tbl-linkman/queryLinkmanById/${id}`,
      method: 'get',
    })
  },
  // 查找所有的供应商信息
  getSupplierOptions(){
    return request({
      url: `/tbl-linkman/getSupplierOptions`,
      method: 'get',
    })
  }
}

