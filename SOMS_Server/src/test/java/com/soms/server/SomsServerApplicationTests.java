package com.soms.server;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author GuoWei qq:1677488547
 * @date 2020/6/11 16:10
 * @version 1.0
 */
@SpringBootTest
public class SomsServerApplicationTests {
    @Test
    void contextLoads() {
    }
}
