package com.soms.server.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.soms.server.entity.TblSupplierComponent;
import com.baomidou.mybatisplus.extension.service.IService;
import com.soms.server.entity.vo.SupplierComponentVo;

import java.util.List;

/**
 * <p>
 * 供应商零件关系表 服务类
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-11
 */
public interface TblSupplierComponentService extends IService<TblSupplierComponent> {
    /**
     * 查询供应商提供零件信息
     * @param page 当前页的信息
     * @return 零件信息
     */
    public Page<TblSupplierComponent> queryAllForPage(Page page);

    /**
     * 供应商添加提供的零件
     * @param tblSupplierComponent 厂商提供零件信息
     * @return SQL执行行数
     */
    public int addTblCompoent(TblSupplierComponent tblSupplierComponent) throws Exception;

    /**
     * 查询供应商供应零件信息,多表查询
     * @param page 当前页的信息
     * @return 零件信息
     */
    Page<SupplierComponentVo> pageSupplierComponent(Page page);

    /**
     * 根据id删除供应商零件信息,逻辑删除
     * @param id 当前页的信息
     * @return sql影响行数，可以用来标识是否成功删除
     */
    int deletesupplierComponentById(Integer id);

    /**
     * 根据零件id查询供应商供应零件，用于修改推荐订单时选择供应商
     * @param componentId  零件id
     * @return 某个零件对应的供应商供应零件信息
     */
    List<SupplierComponentVo> querySupplierComponentByComponentId(int componentId);

    /**
     * 根据id查询供应商零件信息
     * @param id
     * @return  供应商供应的零件信息
     */
    SupplierComponentVo querySupplierCompentById(Integer id);

    /**
     *
     * @param supplierComponentVo  前端传过来的供应商供应零件信息
     * @return sql影响行数，可以用来标识是否成功删除
     */
    int updateSupplierComponentById(SupplierComponentVo supplierComponentVo);
}
