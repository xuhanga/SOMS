package com.soms.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.soms.server.entity.TblOrder;
import com.soms.server.entity.TblSupplier;
import com.soms.server.entity.TblSupplierComponent;
import com.soms.server.entity.vo.ComponentVo;
import com.soms.server.entity.vo.SupplierComponentVo;
import com.soms.server.mapper.TblOrderMapper;
import com.soms.server.mapper.TblSupplierComponentMapper;
import com.soms.server.service.TblComponentService;
import com.soms.server.service.TblOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.soms.server.service.TblSupplierService;
import com.soms.server.util.Algorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订购商表 服务实现类
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-11
 */
@Service
public class TblOrderServiceImpl extends ServiceImpl<TblOrderMapper, TblOrder> implements TblOrderService {

    @Autowired
    private TblOrderMapper tblOrderMapper;
    @Autowired
    TblSupplierComponentMapper tblSupplierComponentMapper;
    @Autowired
    TblSupplierService tblSupplierService;
    @Autowired
    TblComponentService tblComponentService;
    @Override
    public int addOeder(TblOrder tblOrder) {
        int insert = tblOrderMapper.insert(tblOrder);
        return insert;
    }

    @Override
    public int updateOrdeInfo(TblOrder tblOrder) {
        int update = tblOrderMapper.updateById(tblOrder);
        return update;
    }

    @Override
    public TblOrder queryOrder(int orderId) {
        TblOrder tblOrder = tblOrderMapper.selectById(orderId);
        return tblOrder;
    }

    @Override
    public Map recommendOrder(List<ComponentVo> componentVos) {
        // 供应商供应零件信息,算法需要
        Page<SupplierComponentVo> page = new Page<>();
        QueryWrapper<SupplierComponentVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sc.is_deleted",0);
        List<SupplierComponentVo> list = tblSupplierComponentMapper.queryAllSupplierComponent(page,queryWrapper).getRecords();
        // 零件种类的个数,算法需要
        int n = tblSupplierComponentMapper.countComponent();
        // 调用算法
        Map map = Algorithm.test(componentVos,list,n);
        List<SupplierComponentVo> list1 = (List<SupplierComponentVo>) map.get("list");
        for (SupplierComponentVo supplierComponentVo : list1) {
            TblSupplier tblSupplier = tblSupplierService.getBaseMapper().selectById(supplierComponentVo.getSupplierId());
            QueryWrapper<TblSupplierComponent> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("component_id",supplierComponentVo.getComponentId());
            queryWrapper1.eq("supplier_id",supplierComponentVo.getSupplierId());
            Integer id = tblSupplierComponentMapper.selectOne(queryWrapper1).getId();
            supplierComponentVo.setId(id);
            supplierComponentVo.setComponentName(tblComponentService.getBaseMapper().selectById(supplierComponentVo.getComponentId()).getName()) ;
            supplierComponentVo.setSupplierName(tblSupplier.getName()) ;
        }
        map.put("list",list1);
        return map;
    }
}
