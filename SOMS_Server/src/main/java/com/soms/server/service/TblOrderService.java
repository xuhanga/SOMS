package com.soms.server.service;

import com.soms.server.entity.TblOrder;
import com.baomidou.mybatisplus.extension.service.IService;
import com.soms.server.entity.vo.ComponentVo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订购商表 服务类
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-11
 */
public interface TblOrderService extends IService<TblOrder> {
    /**
     * 添加订购商信息
     * @param tblOrder
     * @return
     */
    public int addOeder(TblOrder tblOrder);

    /**
     * 更新订购商信息
     * @param tblOrder 必须有订购商id值，根据id更新
     * @return
     */
    public int updateOrdeInfo(TblOrder tblOrder);

    /**
     * 根据订购商id查询订购商信息
     * @param orderId 订购商id
     * @return 订购商信息
     */
    public TblOrder queryOrder(int orderId);

    /**
     * 根据订购商id查询订购商信息
     * @param componentVos 所需零件种类和数量，封装的实体类
     * @return 推荐订单信息
     */
     Map recommendOrder(List<ComponentVo> componentVos);
}
