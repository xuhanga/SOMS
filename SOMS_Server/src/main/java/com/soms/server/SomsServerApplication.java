package com.soms.server;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author GuoWei qq:1677488547
 * @date 2020/6/11 16:03
 * @version 1.0
 */
@MapperScan(basePackages = {"com.soms.server.mapper"})
@EnableSwagger2
@SpringBootApplication
public class SomsServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(SomsServerApplication.class, args);
    }
}
