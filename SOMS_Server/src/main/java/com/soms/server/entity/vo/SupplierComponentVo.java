package com.soms.server.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SupplierComponentVo {
    private Integer id;
    private Integer componentId; // 零件id
    private Integer supplierId; // 供应商id
    private String componentName;
    private String supplierName;
    private int count;  // 用于返回推荐订单，返回数量
    private BigDecimal price; //价钱

    public SupplierComponentVo(Integer id,int componentId, int supplierId, BigDecimal price,int count) {
        this.id = id;
        this.componentId = componentId;
        this.supplierId = supplierId;
        this.price = price;
        this.count = count;
    }
    public SupplierComponentVo(int componentId, int supplierId, BigDecimal price,int count) {
        this.componentId = componentId;
        this.supplierId = supplierId;
        this.price = price;
        this.count = count;
    }
    public SupplierComponentVo(int componentId, int supplierId, BigDecimal price) {
        this.componentId = componentId;
        this.supplierId = supplierId;
        this.price = price;
    }


}