package com.soms.server.entity.vo;

import com.soms.server.entity.TblLinkman;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class LinkmanVo extends TblLinkman {
    @ApiModelProperty(value = "所属供应商名字")
    private String supplierName;
}
