package com.soms.server.mapper;

import com.soms.server.entity.TblComponent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 零件表 Mapper 接口
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-11
 */
@Repository
public interface TblComponentMapper extends BaseMapper<TblComponent> {

}
