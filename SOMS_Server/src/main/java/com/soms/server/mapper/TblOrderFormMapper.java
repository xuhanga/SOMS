package com.soms.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.soms.server.entity.TblOrderForm;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 订单表是订购商下单的表，内容以列存储的形式进行存储，同一订单编号视为同一订单里的内容。 Mapper 接口
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-15
 */
@Repository
public interface TblOrderFormMapper extends BaseMapper<TblOrderForm> {

}
